angular.module('redditreader', [
  // Declare here all AngularJS dependencies that are shared by the example module.
  'supersonic', 'infinite-scroll', 'ngTouch', 'ngMaterial', 'videosharing-embed'
]).run(function (supersonic) {
    var reddit = new window.Snoocore({
        // You should set a custom user agent for your application
        userAgent: 'reddit-reader',

        // The default wait is 1 request per 2 seconds. If you use OAuth
        // for authentication, you can set this to 1 request per second (1000ms)
        // To disable, set to 0
        throttle: 2000,

        // See the oauth login section for more information
        oauth: {
            type: 'implicit', // required when using implicit OAuth
            mobile: true, // defaults to false.
            consumerKey: 'reyPDxTEMQNgGA',
            redirectUri: 'http://localhost/app/redditreader/login.html',
            scope: ['identity', 'vote', 'subscribe', 'submit', 'read'] // make sure to set all the scopes you need.
        }
    });

    var admobid = {};
    if (/(android)/i.test(navigator.userAgent)) { // for android
        admobid = {
            banner: 'ca-app-pub-4613357769403184/8442798753', // or DFP format "/6253334/dfp_example_ad"
            interstitial: 'ca-app-pub-xxx/yyy'
        };
    } else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) { // for ios
        admobid = {
            banner: 'ca-app-pub-xxx/zzz', // or DFP format "/6253334/dfp_example_ad"
            interstitial: 'ca-app-pub-xxx/kkk'
        };
    } else { // for windows phone
        admobid = {
            banner: 'ca-app-pub-xxx/zzz', // or DFP format "/6253334/dfp_example_ad"
            interstitial: 'ca-app-pub-xxx/kkk'
        };
    }


    if (window.AdMob !== undefined) {
        AdMob.createBanner({
            adId: admobid.banner,
            position: AdMob.AD_POSITION.BOTTOM_CENTER,
            autoShow: false,
            overlap: false
        })
    };


    window.reddit = reddit;
    window.modalImageView = new supersonic.ui.View("redditreader#imageModal");
    window.modalVideoView = new supersonic.ui.View("redditreader#videoModal");
    window.subreddit = new supersonic.ui.View("redditreader#subreddit");
    window.commentsView = new supersonic.ui.View("redditreader#comments");
    window.currentSubreddit = "funny";
    window.admobid = admobid;
    window.alert = null;
}).config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}]);

function pushItems(slice, $scope, callback) {
    angular.forEach(slice.children, function (value, key) {
        var item = {
            isGallery: false,
            isSelf: false,
            isDomain: false,
            isYoutube: false,
            isVideo: false,
            isGif: false,
            isImage: false,
        };
        var url = null;
        if (value.kind == "t3") {
            item.thumbnail = value.data.thumbnail;
            item.title = value.data.title;
            item.ups = value.data.ups;
            item.num_comments = value.data.num_comments;
            item.created = moment.unix(value.data.created_utc).utc().fromNow();
            url = value.data.url;
            item.id = value.data.id;
            item.subreddit = value.data.subreddit;
            item.author = value.data.author;
            item.media = value.data.media;
            item.permalink = value.data.permalink;
            if (item.thumbnail == "self") {
                item.isSelf = true;
                if (value.data.selftext.length > 400) {
                    item.selfText = "Load More";
                } else {
                    item.selfText = value.data.selftext;
                }
            }

            if (url.match(/\.(jpeg|jpg|png)$/)) {
                item.url = url;
                item.isImage = true;

            } else if (url.match(/\.(gif)$/)) {
                item.isGif = true;
                item.url = url;
                $scope.id = Date.now() + Math.floor((Math.random() * 100) + 1);
            } else if (url.match(/\.(gifv)$/)) {
                item.url = url.toString().split('.gif')[0] + ".gif";
                item.isGif = true;
                $scope.id = Date.now() + Math.floor((Math.random() * 100) + 1);
            } else if ((url.indexOf("imgur") > -1) && (url.indexOf("gallery") > -1) || (url.indexOf("/a/") > -1)) {
                item.isGallery = true;
                item.url = url;
            } else if (url.indexOf("imgur") > -1 && !url.match(/\.(jpeg|gif|jpg|png)$/)) {
                item.url = "http://i.imgur.com/" + url.toString().split('com/')[1] + ".jpg";
                item.isImage = true;
            } else if (url.match(/\.(jpeg?|jpg?|png?)$/)) {
                item.url = url.split("?")[0];
                item.isImage = true;
            } else if (url.match(/\.(gif?)$/)) {
                item.url = url.split("?")[0];
                item.isGif = true;
            } else if (url.indexOf('vimeo') > -1 | url.indexOf('dailymotion') > -1 | url.indexOf('youku') > -1) {
                item.isVideo = true;
                item.url = url;
            } else if (url.indexOf('youtube') > -1 | url.indexOf('youtu') > -1) {
                item.isYoutube = true;
                item.url = url;
            } else {
                if (value.data.domain.indexOf('self') > -1) {
                    item.isSelf = true;
                    if (value.data.selftext.length > 400) {
                        item.selfText = "Load More";
                    } else {
                        item.selfText = value.data.selftext;
                    }
                    item.url = url;
                } else {
                    item.isDomain = true;
                    item.domain = value.data.domain;
                    item.url = url;
                }
            }
        }
        $scope.$apply($scope.children.push(item));


    });

    return slice.next().then(callback);
}

document.addEventListener("deviceready", function () {
    var isAndroid = (window.device.platform == 'Android') ? true : false;
    localStorage.setItem('isAndroid', isAndroid);
}, false);