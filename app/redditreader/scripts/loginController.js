angular
    .module('redditreader')
    .controller('loginController', function ($scope, supersonic) {

        $scope.navbarTitle = "Login";

        $scope.isLoggedin = window.localStorage.getItem('isLoggedin');
        if ($scope.isLoggedin == null) {
            $scope.isLoggedin = "false";
        };
        $scope.user = window.localStorage.getItem('user_data');
        var token = window.localStorage.getItem('reddit_accessToken');
        var authUrl = "https://www.reddit.com/api/v1/authorize.compact?client_id=reyPDxTEMQNgGA&state=redditreader&redirect_uri=http%3A%2F%2Flocalhost%2Fapp%2Fredditreader%2Flogin.html&duration=permanent&response_type=code&scope=identity%2Cvote%2Csubscribe%2Csubmit%2Cread";

        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            var appInBrowser = window.open(authUrl, '_blank', 'location=no,toolbar=yes');
            var userDenied = false;
            appInBrowser.addEventListener('loadstart', function (location) {
                if (location.url.indexOf("code=") !== -1) {
                    // Success
                    var access_token = location.url.match(/code=(.*)$/)[1];
                    window.localStorage.setItem('reddit_accessToken', access_token);
                    reddit.auth(access_token).then(function () {
                        return reddit('/api/v1/me').get();
                    }).then(function (data) {
                        window.localStorage.setItem('user_data', JSON.stringify(data));
                        $scope.isLoggedin = true;
                        window.localStorage.setItem('isLoggedin', "true");
                    });
                    appInBrowser.close();
                }

                if (location.url.indexOf("error") !== -1) {
                    // User denied
                    userDenied = true;
                    window.localStorage.setItem('reddit_accessToken', null);
                    window.localStorage.setItem('isLoggedin', "false");
                    //appInBrowser.close();
                    console.log(location);
                }
            });
        };

        $scope.doLogout = function () {
            reddit.deauth();
            window.localStorage.setItem('isLoggedin', "false");
            window.localStorage.setItem('reddit_accessToken', null);
            window.localStorage.setItem('user_data', null);
        }

    });