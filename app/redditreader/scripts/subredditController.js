angular
    .module('redditreader')
    .controller('subredditController', function ($scope, supersonic) {

        /*        if (window.localStorage.getItem('reddit_accessToken') !== null) {
                    var access_token = window.localStorage.getItem('reddit_accessToken');
                    reddit.auth(access_token).then(function () {
                        return reddit('/api/v1/me').get();
                    }).then(function (data) {
                        window.localStorage.setItem('user_data', JSON.stringify(data));
                        window.localStorage.setItem('isLoggedin', "true");
                    });
                }*/

        if (window.AdMob !== undefined) {
            AdMob.showBanner(AdMob.AD_POSITION.BOTTOM_CENTER);
        };

        $scope.openModal = function (child) {
            var obj = JSON.stringify(child);
            localStorage.setItem('child', obj);

            //supersonic.ui.modal.show(window.modalImageView);
            supersonic.ui.layers.push(window.modalImageView);
        }

        $scope.openVideoModal = function (child) {
            var obj = JSON.stringify(child);
            localStorage.setItem('child', obj);
            supersonic.ui.layers.push(window.modalVideoView);
        }

        $scope.openWebView = function (child) {
            var webview = new steroids.views.WebView(child.url);
            steroids.layers.push(webview);
            if (window.AdMob !== undefined) {
                AdMob.showBanner(AdMob.AD_POSITION.BOTTOM_CENTER);
            };
        }

        $scope.vote = function (id, $event) {
            $($event.currentTarget).css("color", "#00B5FF");
            reddit('/api/vote').post({
                dir: 1,
                id: id
            }).then(function (result) {
                console.log(result);
            }, function (error) {
                console.log("error: " + error);
            });
        }

        $scope.share = function (child) {
            if (child.isImage) {
                window.plugins.socialsharing.share(null, null, child.url, null);
            } else {
                window.plugins.socialsharing.share(null, null, null, child.url);
            }

        }

        var callback = function (buttonIndex) {
            setTimeout(function () {
                var category = "/hot";
                if (buttonIndex == 1) {
                    category = "/hot";
                } else if (buttonIndex == 2) {
                    category = "/new";
                } else if (buttonIndex == 3) {
                    category = "/rising";
                } else if (buttonIndex == 4) {
                    category = "/controversial";
                } else if (buttonIndex == 5) {
                    category = "/top";
                }
                var sub = "/r/" + localStorage.getItem('subreddit') + category;
                $scope.navbarTitle = sub;
                if (localStorage.getItem('subreddit') == "home") {
                    sub = category;
                    $scope.navbarTitle = "Home" + category;
                }
                $(window).scrollTop(0);
                window.reddit(sub).listing({
                    // Any parameters for the endpoints can be used here as usual.
                    // In this case, we specify a limit of 10 children per slice
                    limit: 10
                }).then(function (slice) {
                    $scope.children = [];
                    $scope.limit = 10;
                    var callback = function (nextSlice) {

                        $scope.limit += 10;
                        $scope.loadMore = function () {
                            pushItems(nextSlice, $scope, callback);
                        }
                    }
                    pushItems(slice, $scope, callback);
                    console.log(slice.children); // The children that are not stickied
                });
            });
        };

        $scope.showCategories = function () {
            var options = {
                'title': 'Choose a sub-category...',
                'buttonLabels': ['hot', 'new', 'rising', 'controversial', 'top'],
                'androidEnableCancelButton': true, // default false
                'winphoneEnableCancelButton': true, // default false
                'addCancelButtonWithLabel': 'Cancel',
            };
            window.plugins.actionsheet.show(options, callback);
        }

        $scope.showComments = function (child) {
            var obj = JSON.stringify(child);
            localStorage.setItem('child', obj);
            supersonic.ui.layers.push(window.commentsView);
        }

        var sub = "/r/" + localStorage.getItem('subreddit') + '/hot';
        $scope.navbarTitle = sub;
        if (localStorage.getItem('subreddit') == "home") {
            sub = "/";
            $scope.navbarTitle = "Home";
        }

        window.reddit(sub).listing({
            // Any parameters for the endpoints can be used here as usual.
            // In this case, we specify a limit of 10 children per slice
            limit: 10
        }).then(function (slice) {
            $scope.children = [];
            $scope.limit = 10;
            var callback = function (nextSlice) {

                $scope.limit += 10;
                $scope.loadMore = function () {
                    pushItems(nextSlice, $scope, callback);
                }
            }
            pushItems(slice, $scope, callback);
            console.log(slice.children); // The children that are not stickied
        });

    });