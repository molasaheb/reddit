angular
    .module('redditreader')
    .controller('drawerController', function ($scope, supersonic) {

        $scope.navbarTitle = "Menu";

        $scope.openSubreddit = function (sub) {
            supersonic.ui.drawers.close();
            localStorage.setItem('subreddit', sub);
            supersonic.ui.layers.push(window.subreddit);
        }

    });