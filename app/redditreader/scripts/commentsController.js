angular
    .module('redditreader')
    .controller('commentsController', function ($scope, supersonic) {

        $scope.navbarTitle = "Comments";
        var obj = JSON.parse(localStorage.getItem('child'));
        var subreddit = localStorage.getItem('subreddit');
        console.log(obj.permalink);
        var url = "/r/" + subreddit + "/comments/" + obj.id;
        window.reddit(url).get({
            context: 8,
            showedits: false,
            showmore: true,
            sort: 'hot'
        }).then(function (slice) {
            console.log(slice); // The children that are not stickied
        });

        $scope.delete = function (data) {
            data.nodes = [];
        };
        $scope.add = function (data) {
            var post = data.nodes.length + 1;
            var newName = data.name + '-' + post;
            data.nodes.push({
                name: newName,
                nodes: []
            });
        };
        $scope.tree = [{
            name: "Node",
            nodes: []
        }];
    });