angular
    .module('redditreader')
    .controller('imageController', function ($scope, supersonic, $sce) {
        var obj = JSON.parse(localStorage.getItem('child'));
        $scope.navbarTitle = obj.title;
        if (obj.isGif) {
            var img = new Image();
            img.className = "full-image";
            img.src = obj.url;
            img.onload = function () {
                document.getElementById("gifPlaceHolder").innerHTML = "";
                document.getElementById("gifPlaceHolder").appendChild(img);
            };
        }
        $scope.item = obj;
        $scope.height = window.innerHeight - 100;
        $scope.width = window.innerWidth - 20;
    
        
        $scope.close = function () {
            supersonic.ui.layers.pop();
            //supersonic.ui.modal.hide();
        }
    });