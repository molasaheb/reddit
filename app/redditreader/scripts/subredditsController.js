angular
    .module('redditreader')
    .controller('subredditsController', function ($scope, supersonic) {
        // get users subreddits
        $scope.userSubreddits = JSON.parse(localStorage.getItem('userSubreddits'));

        $scope.openSubreddit = function (sub) {
            localStorage.setItem('subreddit', sub);
            supersonic.ui.layers.push(window.subreddit);
        }

        $scope.edit = function (sub, event, index) {
            event.preventDefault();
            event.stopPropagation();
            var options = {
                message: "Edit subreddit name",
                buttonLabels: ["Save", "Delete"],
                defaultText: sub.id
            };

            supersonic.ui.dialog.prompt("Edit", options).then(function (result) {
                if (result.buttonIndex == 1) {
                    $scope.userSubreddits.splice(index, 1);
                    var userSubreddits = JSON.stringify($scope.userSubreddits);
                    localStorage.setItem('userSubreddits', userSubreddits);
                    $scope.$apply();
                } else {
                    var subreddit = {
                        id: result.input
                    };
                    $scope.userSubreddits[index] = subreddit;
                    localStorage.setItem('userSubreddits', JSON.stringify($scope.userSubreddits));
                    $scope.$apply();
                };
            });
        }

        $scope.addSubreddit = function () {
            var options = {
                message: "Please type a subreddit name to add",
                buttonLabels: ["Save"],
                defaultText: "subreddit name"
            };

            supersonic.ui.dialog.prompt("Add subreddits", options).then(function (result) {
                if ($scope.userSubreddits == null) {
                    $scope.userSubreddits = [];
                }
                if (result.input !== "subreddit name") {
                    var id = $scope.userSubreddits.length + 1;
                    var subreddit = {
                        id: result.input
                    };
                    $scope.userSubreddits.push(subreddit);
                    localStorage.setItem('userSubreddits', JSON.stringify($scope.userSubreddits));
                    $scope.$apply();
                }
            });
        }
    });