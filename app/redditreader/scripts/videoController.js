angular
    .module('redditreader')
    .controller('videoController', function ($scope, supersonic) {
        $scope.isAndroid = JSON.parse(localStorage.getItem('isAndroid'));
        var obj = JSON.parse(localStorage.getItem('child'));
        $scope.navbarTitle = obj.title;
        $scope.item = obj;
        $scope.height = window.innerHeight - 100;
        $scope.width = window.innerWidth - 20;
        $scope.openYoutube = function (url) {
            window.plugins.videoPlayer.play(url);
        }
        $scope.close = function () {
            supersonic.ui.layers.pop();
        }
        if ($scope.isAndroid) {
            $scope.openYoutube(obj.url);
        }
    });