# Read more about app structure at http://docs.appgyver.com

module.exports =

  # See styling options for tabs and other native components in app/common/native-styles/ios.css or app/common/native-styles/android.css

   drawers:
     left:
       id: "leftDrawer"
       location: "redditreader#drawer"
       showOnAppLoad: false
     options:
       animation: "swingingDoor"

   rootView:
     location: "redditreader#subreddits"

  preloads: [
    {
        id: "subreddits"
        location: "redditreader#subreddits"
    }
    {
        id: "login"
        location: "redditreader#login"
    }
  ]


